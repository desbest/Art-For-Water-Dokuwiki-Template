# Art For Water dokuwiki template

* Designed by [Jessica Russell Flint](http://jessicarussellflint.co.uk)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:artforwater)

![art for water theme screenshot](https://i.imgur.com/HuhMdvi.png)